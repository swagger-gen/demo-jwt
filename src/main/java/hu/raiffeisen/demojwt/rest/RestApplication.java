package hu.raiffeisen.demojwt.rest;

import hu.raiffeisen.demojwt.rest.api.impl.LoginApiServiceImpl;
import hu.raiffeisen.demojwt.rest.api.impl.UsersApiServiceImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class RestApplication extends Application {


    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<Class<?>>();
        resources.add(LoginApiServiceImpl.class);
        resources.add(UsersApiServiceImpl.class);

        return resources;
    }


}