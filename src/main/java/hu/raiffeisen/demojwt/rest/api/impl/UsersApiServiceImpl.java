package hu.raiffeisen.demojwt.rest.api.impl;

import hu.raiffeisen.demojwt.rest.api.UsersApi;
import hu.raiffeisen.demojwt.service.UserService;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-22T14:34:05.289+01:00")
public class UsersApiServiceImpl implements UsersApi {
    public Response getUsers(SecurityContext securityContext) {
        return Response.ok(UserService.getUsers()).build();
    }
}
