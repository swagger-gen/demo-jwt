package hu.raiffeisen.demojwt.rest.api.impl;

import hu.raiffeisen.demojwt.rest.api.LoginApi;
import hu.raiffeisen.demojwt.rest.model.LoginDTO;
import hu.raiffeisen.demojwt.service.LoginService;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-22T14:34:05.289+01:00")
public class LoginApiServiceImpl implements LoginApi {
    public Response login(LoginDTO loginDTO, SecurityContext securityContext) {
        return Response.ok(LoginService.login(loginDTO)).build();
    }
}
