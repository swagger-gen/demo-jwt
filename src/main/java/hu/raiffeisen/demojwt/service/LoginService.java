package hu.raiffeisen.demojwt.service;

import hu.raiffeisen.demojwt.rest.model.LoginDTO;
import hu.raiffeisen.demojwt.rest.model.LoginResponseDTO;
import hu.raiffeisen.demojwt.rest.model.LoginResponseDTOName;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

public class LoginService {
    public static LoginResponseDTO login(LoginDTO loginDTO) {
        LoginResponseDTO res = new LoginResponseDTO();
        LoginResponseDTOName name = new LoginResponseDTOName();
        name.setFirst("Teszt");
        name.setLast(loginDTO.getUsername());
        res.setName(name);
        res.setBirthDate(new LocalDate());
        res.setLoginTime(new DateTime());
        res.setData(new byte[]{(byte) 0x00, (byte) 0x01, (byte) 0x02});
        res.setSuccess(true);

        res.setToken(JwtService.createToken(loginDTO.getUsername()));
        return res;
    }
}
