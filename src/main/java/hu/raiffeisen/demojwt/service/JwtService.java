package hu.raiffeisen.demojwt.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtService {

    private static final String ISSUER = "Raiffeisen Bank";
    private static final String SECRET = "secret";


    public static String createToken(String userName) {
        String token = null;
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            Map<String, Object> header = new HashMap<>();
            header.put("typ", "JWT");

            Date expDate = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(expDate);
            c.add(Calendar.SECOND, 30);
            expDate = c.getTime();


            token = JWT.create()
                    .withHeader(header)
                    .withIssuer(ISSUER)
                    .withSubject(userName)
                    .withIssuedAt(new Date())
                    .withExpiresAt(expDate)
                    .sign(algorithm);
        } catch (UnsupportedEncodingException exception) {
            System.out.println(exception.getMessage());
        } catch (JWTCreationException exception) {
            System.out.println(exception.getMessage());
        }
        return token;
    }

    public static void validateToken(String token) throws Exception {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);
        JWTVerifier verifier = com.auth0.jwt.JWT.require(algorithm)
                .withIssuer(ISSUER)
                .build(); //Reusable verifier instance
        DecodedJWT jwt = verifier.verify(token);
        System.out.println("Token OK " + jwt.getSubject());
    }


}
