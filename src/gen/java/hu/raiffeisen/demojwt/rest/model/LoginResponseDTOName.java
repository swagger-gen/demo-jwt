package hu.raiffeisen.demojwt.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-24T11:15:52.125+01:00")
public class LoginResponseDTOName implements Serializable {

    private String first = null;
    private String last = null;

    /**
     **/

    @ApiModelProperty(example = "null", required = true, value = "")
    @JsonProperty("first")
    @NotNull
    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    /**
     **/

    @ApiModelProperty(example = "null", required = true, value = "")
    @JsonProperty("last")
    @NotNull
    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoginResponseDTOName loginResponseDTOName = (LoginResponseDTOName) o;
        return Objects.equals(first, loginResponseDTOName.first) &&
                Objects.equals(last, loginResponseDTOName.last);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, last);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class LoginResponseDTOName {\n");

        sb.append("    first: ").append(toIndentedString(first)).append("\n");
        sb.append("    last: ").append(toIndentedString(last)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

