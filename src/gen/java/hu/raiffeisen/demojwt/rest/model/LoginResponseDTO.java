package hu.raiffeisen.demojwt.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-24T14:12:30.000+01:00")
public class LoginResponseDTO implements Serializable {
  
  private LoginResponseDTOName name = null;
  private String token = null;
  private LocalDate birthDate = null;
  private DateTime loginTime = null;
  private byte[] data = null;
  private Boolean success = null;

  /**
   **/

  @ApiModelProperty(example = "null", value = "")
  @JsonProperty("name")
  public LoginResponseDTOName getName() {
    return name;
  }
  public void setName(LoginResponseDTOName name) {
    this.name = name;
  }

  /**
   **/

  @ApiModelProperty(example = "null", required = true, value = "")
  @JsonProperty("token")
  @NotNull
  public String getToken() {
    return token;
  }
  public void setToken(String token) {
    this.token = token;
  }

  /**
   **/

  @ApiModelProperty(example = "null", required = true, value = "")
  @JsonProperty("birthDate")
  @NotNull
  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  /**
   **/

  @ApiModelProperty(example = "null", required = true, value = "")
  @JsonProperty("loginTime")
  @NotNull
  public DateTime getLoginTime() {
    return loginTime;
  }

  public void setLoginTime(DateTime loginTime) {
    this.loginTime = loginTime;
  }

  /**
   **/

  @ApiModelProperty(example = "null", required = true, value = "")
  @JsonProperty("data")
  @NotNull
  public byte[] getData() {
    return data;
  }
  public void setData(byte[] data) {
    this.data = data;
  }

  /**
   **/

  @ApiModelProperty(example = "null", required = true, value = "")
  @JsonProperty("success")
  @NotNull
  public Boolean getSuccess() {
    return success;
  }
  public void setSuccess(Boolean success) {
    this.success = success;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoginResponseDTO loginResponseDTO = (LoginResponseDTO) o;
    return Objects.equals(name, loginResponseDTO.name) &&
            Objects.equals(token, loginResponseDTO.token) &&
            Objects.equals(birthDate, loginResponseDTO.birthDate) &&
            Objects.equals(loginTime, loginResponseDTO.loginTime) &&
            Objects.equals(data, loginResponseDTO.data) &&
            Objects.equals(success, loginResponseDTO.success);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, token, birthDate, loginTime, data, success);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LoginResponseDTO {\n");

    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
    sb.append("    loginTime: ").append(toIndentedString(loginTime)).append("\n");
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

