package hu.raiffeisen.demojwt.rest.api;

import hu.raiffeisen.demojwt.rest.model.LoginDTO;
import hu.raiffeisen.demojwt.rest.model.LoginResponseDTO;
import io.swagger.annotations.ApiParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/login")
@Consumes({"application/json"})
@Produces({"application/json"})
@io.swagger.annotations.Api(description = "the login API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-22T14:34:05.289+01:00")
public interface LoginApi {

    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "", notes = "Bejelentkezés", response = LoginResponseDTO.class, tags = {"Login",})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Sikeres regisztráció", response = LoginResponseDTO.class)})
    public Response login(@ApiParam(value = "") LoginDTO loginDTO, @Context SecurityContext securityContext);
}
